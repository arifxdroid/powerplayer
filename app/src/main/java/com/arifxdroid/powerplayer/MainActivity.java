package com.arifxdroid.powerplayer;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private ArrayList<Song> songList;
    private ListView listView;
    private SongService songService;
    private Intent musicIntent;
    private boolean musicBound = false;

    private Button btnPlay,btnFB,btnFF;
    private TextView txtSong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();


        txtSong = (TextView)findViewById(R.id.txtSong);
        songList = new ArrayList<Song>();
        listView = (ListView)findViewById(R.id.listView);
        btnPlay = (Button)findViewById(R.id.btnPlay);

        btnFB = (Button)findViewById(R.id.btnFB);
        btnFF = (Button)findViewById(R.id.btnFF);

        getListSongs();

        CustomSongAdapter adapter = new CustomSongAdapter(this,songList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                btnPlay.append("");
                if (songService.songIsPlaying()){
                    songService.stopOnly();
                    songService.releaseOnly();
                    songService.setSong(i);
                    songService.playMusic(getApplicationContext());
                    txtSong.append(songList.get(i).getTitle().toString());
                    btnPlay.setText("||");
                }
                else {
                    songService.setSong(i);
                    songService.playMusic(getApplicationContext());
                    txtSong.append(songList.get(i).getTitle().toString());
                    btnPlay.setText("||");
                }
            }
        });

        btnPlay.setOnClickListener(this);
        btnFB.setOnClickListener(this);
        btnFF.setOnClickListener(this);


    }

    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            SongService.MusicBinder binder = (SongService.MusicBinder)iBinder;
            songService = binder.getService();
            songService.setList(songList);
            musicBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            musicBound = false;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        if (musicIntent == null){
            musicIntent = new Intent(this, SongService.class);
            bindService(musicIntent,musicConnection, Context.BIND_AUTO_CREATE);
            startService(musicIntent);
        }
    }

//    public void songPiked(View view){
//        songService.setSong(Integer.parseInt(view.getTag().toString()));
//        songService.playMusic();
//    }

    public void getListSongs(){
        ContentResolver songsResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor songCursor = songsResolver.query(songUri,null,null,null,null);

        if (songCursor!=null&songCursor.moveToFirst()){
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int columnId = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int artistColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);

            do {
                long id = songCursor.getLong(columnId);
                String title = songCursor.getString(titleColumn);
                String artist = songCursor.getString(artistColumn);
                songList.add(new Song(id,title,artist));
            }while (songCursor.moveToNext());
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btnPlay:
             //   songService.setSong(Integer.parseInt(view.getTag().toString()));
             //  songService.playMusic();
                if (songService.songIsPlaying()){
                    songService.pauseOnly();
                    btnPlay.setText(">");
                }else {
                    songService.playOnly();
                    btnPlay.setText("||");
                }

                break;

            case R.id.btnFF:
                songService.firstForward();
                break;

            case R.id.btnFB:
                songService.firstBackward();
                break;
        }

    }
}
