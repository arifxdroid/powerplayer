package com.arifxdroid.powerplayer;

import android.app.Service;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.MediaStore;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Mobile App Develop on 27-5-15.
 */
public class SongService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private  MediaPlayer mPlayer;
    private ArrayList<Song> songs;
    private  int songPosition;
    private  final  IBinder musicBinder= new MusicBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        mPlayer = new MediaPlayer();
        songPosition = 0;
        initializePlayer();
    }

    public void initializePlayer(){

        mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnCompletionListener(this);
    }

    public class MusicBinder extends Binder{
        SongService getService(){
            return SongService.this;
        }
    }

    public void setList(ArrayList<Song> theSong){
        this.songs = theSong;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return musicBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {

        mPlayer.stop();
        mPlayer.release();
        return false;
    }

    public void playMusic(Context c){


        Song playingSong = songs.get(songPosition);
        long currentSong = playingSong.getId();

        Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currentSong);
//            try {
//                mPlayer.setDataSource(getApplicationContext(), uri);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            mPlayer.prepareAsync();
        mPlayer = MediaPlayer.create(c,uri);
        mPlayer.start();
//        if (mPlayer.isPlaying()){
//            mPlayer.stop();
//            mPlayer.release();
//        }
//        else {
//
//        }

    }

    public boolean songIsPlaying(){
        boolean status = mPlayer.isPlaying();
        return status;
    }

    public void pauseOnly(){
        mPlayer.pause();
    }

    public void playOnly(){
        mPlayer.start();
    }

    public void stopOnly(){
        mPlayer.stop();
    }

    public void releaseOnly(){
        mPlayer.release();
    }

    public void startOnly(){
        mPlayer.start();
    }

    public void nextSong(){
        mPlayer.stop();
        mPlayer.release();
        Song nextSong = ((songs.get(songPosition+1))==null)?songs.get(0):songs.get(songPosition+1);
        long currentSong = nextSong.getId();
        Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currentSong);
        try {
            mPlayer.setDataSource(getApplicationContext(),uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayer.prepareAsync();

    }

    public void prevSong(){
        mPlayer.stop();
        mPlayer.release();
        Song prevSong = ((songs.get(songPosition-1))==null)?songs.get(songs.size()-1):songs.get(songPosition-1);
        long currentSong = prevSong.getId();
        Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currentSong);
        try {
            mPlayer.setDataSource(getApplicationContext(),uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayer.prepareAsync();

    }

    public void firstForward(){
        mPlayer.seekTo(mPlayer.getCurrentPosition()+500);
    }

    public void firstBackward(){
        mPlayer.seekTo(mPlayer.getCurrentPosition()-500);
    }

    public void setSong(int songIndex){
        songPosition = songIndex;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }


}
