package com.arifxdroid.powerplayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mobile App Develop on 27-5-15.
 */
public class CustomSongAdapter extends BaseAdapter {

    private ArrayList<Song> songs;
    Context context;
    private LayoutInflater inflater;

    public CustomSongAdapter(Context c, ArrayList<Song> theSong){
        this.context = c;
        songs = theSong;
        inflater = LayoutInflater.from(context);
    }

    public void myName(){

    }
    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout songLayout = (LinearLayout)inflater.inflate(R.layout.songlist,parent,false);

        TextView txtSongTitle = (TextView)songLayout.findViewById(R.id.txtTitle);
        TextView txtArtistView = (TextView)songLayout.findViewById(R.id.txtArtist);

        Song currentSong = songs.get(position);
        txtSongTitle.setText(currentSong.getTitle());
        txtArtistView.setText(currentSong.getArtist());
        songLayout.setTag(position);
        return songLayout;
    }
}
